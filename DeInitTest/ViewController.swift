//
//  ViewController.swift
//  DeInitTest
//
//  Created by Nayem BJIT on 6/28/17.
//  Copyright © 2017 Nayem BJIT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, Navigator {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        print("First view controller's deinit called")
    }
    
    func passData(data: String) {
        print("In First view controller: \(data)")
    }
    
    @IBAction func gotoSecond(_ sender: UIButton) {
        let viewcontroller = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        viewcontroller.delegate = self
        show(viewcontroller, sender: self)
    }

}

