//
//  SecondViewController.swift
//  DeInitTest
//
//  Created by Nayem BJIT on 6/28/17.
//  Copyright © 2017 Nayem BJIT. All rights reserved.
//

import UIKit

protocol Navigator {
    func passData(data:String)
}

class SecondViewController: UIViewController {
    
    weak var delegate:Navigator?
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    deinit {
        print("Second view controller's deinit called")
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        delegate?.passData(data: "Delegation from second view controller")
        dismiss(animated: true, completion: nil)
    }
    

}
